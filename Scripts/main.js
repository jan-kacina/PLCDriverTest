myscada=require('./myscada');
myscada.init();

let heartBeat = 1;
let startupWriteToPlc = {}
startup();

async function startup(){
  setInterval(writeHeartBeat, 1000);
  setInterval(readPlcIsRunning, 1000);
  let iteration = 0;
  let writeUnsuccessful = true;
  do {
    iteration++;
    try{
      const stationRfidResult1 = await readTagsSymbolicAsync('station_rfidValue');
      const stationRfid1 = stationRfidResult1.value.value;
      const stationRfidResult2 = await readTagsSymbolicAsync('station_rfidValue2');
      const stationRfid2 = stationRfidResult2.value.value;
      startupWriteToPlc[`read${iteration}`] = {result: 'SUCCESS', stationRfid1: stationRfid1, stationRfid2: stationRfid2, timestamp:new Date(Date.now()).toISOString()};
      writeUnsuccessful = false;
    }
    catch (err) {
      startupWriteToPlc[`read${iteration}`] = {result: 'ERROR', timestamp:new Date(Date.now()).toISOString()};
    }
    sleep(50);
  } while (writeUnsuccessful)
}

async function writeHeartBeat() {
  writeToPlc('heartBeat', 'value', heartBeat, console.log);
  if(heartBeat === 1) {
    heartBeat = 0;
  } else {
    heartBeat = 1;
  }
  await sleep(1000);
}

async function readPlcIsRunning() {
  const aaa = readFromPlc('plcIsRunning', 'value', heartBeat, console.log);
  console.log('Read');
}

//process data sent from the view script
//in view script, please, use function myscada.sendDataToServerSideScript
myscada.dataFromViewScripts = function (data,callback)
{
  switch(data.operation){
    case 'writeToPlc':
      writeToPlc(data.params.tableName, data.params.tagName, data.params.tagValue, callback);
      break;
    case 'readFromPlc':
      readFromPlc(data.params.tableName, data.params.tagNames, callback)
      break;
    case 'parallelReadFromPlc':
      parallelReadFromPlc(data.params.tableNames, callback)
      .then(callback);
      break;
    case 'slowAsyncWriteToPlc':
      slowAsyncWriteToPlc(data.params.tableName, data.params.tagValue)
      .then(callback);
      break;
    case 'getStartupWriteToPlc':
      callback(startupWriteToPlc); 
      break;
    default:
      callback(`Unknown operation ${data.operation}`);
  }
};

async function slowAsyncWriteToPlc(tableName, tagValue){
	var options={};
	options['name']=tableName;
	options['values'] = {};
	options['values']['value']=tagValue;

  await writeTagsAsync(options);

  let response = {}
  let readValue = 0
  let iteration = 1
  do {
    readValue = await readTagsSymbolicAsync(tableName);
    response[`read${iteration}`] = {value: readValue.value.value, timestamp:new Date(Date.now()).toISOString()};
    iteration++;
    await sleep(50);
  } while(readValue.value.value !== tagValue);

  return response;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function writeToPlc(tableName, tagName, tagValue, callback){
	var options={};
	options['name']=tableName;
	options['values'] = {};
	options['values'][tagName]=tagValue;

  //const returnValue = JSON.stringify(options);
  //callback(`${returnValue}`);

	myscada.writeTags(options, function (err,data){
		if (err){
      callback(`Server error! Error: ${err}; Data: ${data}`);
		} else {
			callback(`Server success! Data: ${data}`);
		}
	});
}

function readFromPlc(tableName, tagNames, callback) {
  //Read tags for tag group station
  myscada.readTagsSymbolic(tableName, function (err,data){
    if (!err){
      const response = {};
      tagNames.map(n => {
        response[n] = data[n].value
        response[`${n}_err`] = data[n].err
      });
      const responseString = `${tableName} => ${JSON.stringify(data)} => ${JSON.stringify(response)}`;
      callback(responseString);
    }
  });
}

function parallelReadFromPlc(tableNames, callback){
  return new Promise(function(resolve, reject) {
    let promises = [];
    tableNames.map( n => promises.push(
      readTagsSymbolicAsync(n)
      .then(data => {
        return {
          [n]: data.value.value
        }
      })
    ));

    Promise.all(promises)
    .then(data => {
      const response = {};
      
      resolve(data);
    });
  });
}


//wrapper around old-fashioned "nodeback"
//https://stackoverflow.com/questions/22519784/how-do-i-convert-an-existing-callback-api-to-promises
function writeTagsAsync(options){
  return new Promise(function(resolve, reject) {
    myscada.writeTags(options, function(error, response) {
      if(error){
        reject(error);
      }
      else{
        resolve(response);
      }
    })
  });
}

//wrapper around old-fashioned "nodeback"
//https://stackoverflow.com/questions/22519784/how-do-i-convert-an-existing-callback-api-to-promises
function readTagsSymbolicAsync(tableName){
  return new Promise(function(resolve, reject) {
    //resolve(tableName);
    myscada.readTagsSymbolic(tableName, function(err, data) {
      if(err){
        reject(err);
      }
      else{
        //const responseString = `${tableName} => ${JSON.stringify(data)};JK`;
        //resolve(responseString);
        resolve(data);
      }
    })
  });
}